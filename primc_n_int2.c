#include <stdio.h>

int main()
{
    int n,i,j,flag=0,p=0,c=0;
    printf(" Enter number N\n ");
    scanf("%d",&n);
    int array[n];
    printf(" Enter %d numbers\n",n);
    for(i=0; i<n; i++)
    {
        scanf("%d",&array[i]);
    }
    for(i=0; i<n;i++)
    {
      if(array[i]!=1)
      {
        for(j=2; j<=array[i]/2; j++)
        {
            flag=0;
            if(array[i]%j==0)
            {
                flag=1;
                break;
            }
        }
        if(flag==1)
            c++;
        else
            p++;
      }
    }
    printf(" Total number of prime numbers = %d \n Total number of composite numbers = %d\n",p,c);
	return 0;
}
