#include <stdio.h>
void swap(int *x,int *y)
{
     int t;
    t=*x;
    *x= *y;
    *y= t;
}
int main()
{
   int a,b;
   printf("Enter the numbers:\na = ");
   scanf("%d",&a);
   printf("b =");
   scanf("%d",&b);
   swap(&a,&b);
   printf("After swapping:\na= %d\nb= %d\n",a,b);
   return 0;
}