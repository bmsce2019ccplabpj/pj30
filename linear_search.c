#include <stdio.h>
int main()
{
    int i,n,k,p;
    printf("Enter the number of elements in an array:\n");
    scanf("%d",&n);
    int array[n];
    printf("Enter %d elements of the array\n",n);
    for(i=0; i<n; i++)
    {
       scanf("%d",&array[i]);
    }
    printf("Enter the search element:\n");
    scanf("%d",&p);
    for(i=0; i<n; i++)
    {
       if(array[i]==p)
       {
         printf("position of search element is %d",i+1);
         break;
       }
       else
         k=-1;
    }
    if(k==-1)
    {
      printf("Element not found\n");
    }
    return 0;
}