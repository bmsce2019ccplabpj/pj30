#include <stdio.h>
struct employee
{
   int id;
   char name[50];
   long int sal;
   char doj[11];
};
int main()
{
   struct employee e1;
   printf("Employee I.D: ");
   scanf("%d",&e1.id);
   printf("\nEmployee name: ");
   gets(e1.name);
   printf("\nEmployee salary: ");
   scanf("%lf",&e1.sal);
   printf("\nDate of joining(dd/mm/yyyy): ");
   gets(e1.doj);
   printf("\nEmployee details:\n");
  printf("Employee I.D: %d",e1.id);
   printf("\nEmployee name: ");
   puts(e1.name);
   printf("\nEmployee salary: %lf",e1.sal);
   printf("\nDate of joining(dd/mm/yyyy): ");
   puts(e1.doj);  
   return 0;
}