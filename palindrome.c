#include <stdio.h>

int main()
{
    int x,n,r,rev;
	printf("Enter a number\n");
    scanf("%d",&x);
    n=x;
    for(rev = 0; n!=0; n/=10)
    {
        r = n%10;
        rev = rev*10 + r;
    }
    printf("reversed number is %d\n",rev);
    if(x==rev)
    {
        printf("Hence %d is a palindrome\n",x);
    }
    else
    {
        printf("Hence %d is not a palindrome\n",x);
    }   
	return 0;
}
