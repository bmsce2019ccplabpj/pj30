#include <stdio.h>
#include <math.h>
int main()
{
  float a,b,c,r1,r2,d;
  int k;
  printf("enter the coefficients of quadratic equation\n");
  scanf("%f %f %f",&a,&b,&c);
  d= b*b - 4*a*c;
  if(d==0.0)
      k=0;
  else if(d>0.0)
      k=1;
  else
      k=-1;
  switch(k)
  {
    case -1: r1 = -b/(2*a);
             r2 = sqrt(fabs(d))/(2*a);
             printf("roots are imaginary \n r1 = %f+i%f\n r2 = %f-i%f\n",r1,r2,r1,r2);
             break;
    case 0: r1 = -b/(2*a);
            printf("roots are equal\n r1 = r2 = %f\n",r1);
            break;
    case 1: r1 = (-b + sqrt(d))/(2*a);
            r2 = (-b - sqrt(d))/(2*a);
            printf("roots are distinct\n r1 = %f \n r2 = %f\n",r1,r2);
    default: printf("invalid entry\n");        
  }
  return 0;
}